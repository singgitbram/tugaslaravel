<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    protected $table = "pertanyaan"; //kalo mau override nama table
    protected $primaryKey = "pertanyaan_id"; //kalo mau override nama PK

    //untuk bisa mass assignment harus pasang fillable yaitu kolom apa aja yg bs diisi
    protected $fillable = ["judul", "isi"];
    // protected $guarded = [] bisa juga pake $guarded yaitu kebalikan fillable
    public function jawaban()
    {
        return $this->hasMany('App\Jawaban');
    }
    public function profil()
    {
        return $this->belongsTo('App\profil');
    }
    public function tags()
    {
        return $this->belongsToMany('App\Tag','pertanyaan_tag','pertanyaan_id','tag_id');
    }
}
