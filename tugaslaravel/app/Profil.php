<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    protected $table = "profil"; //kalo mau override nama table
    protected $primaryKey = "profil_id"; //kalo mau override nama PK

    public function pertanyaan()
    {
        return $this->hasMany('App\Pertanyaan','profil_id');
    }
}
