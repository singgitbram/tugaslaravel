<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

use DB;
use App\Tag;
use Auth;
use App\Pertanyaan; //supaya bisa eloquent

class PertanyaanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('readData', 'readDataById'); //only('abc')ini kebalikan except
    }

    public function createForm()
    {
        return view('pertanyaan.formPertanyaan');
    }

    public function createData(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
        ]);

        //cara query builder
        // $query = DB::table('pertanyaan')->insert(
        //     [
        //         'judul' => $request['judul'],
        //         'isi' => $request['isi']
        //     ]
        // );

        //cara eloquent standart
        // $pertanyaan = new Pertanyaan;
        // $pertanyaan->judul = $request['judul'];
        // $pertanyaan->isi = $request['isi'];
        // $pertanyaan->save();

        $tag_ids = [];
        $tags_arr = explode(',', $request["tags"]);
        foreach ($tags_arr as $tag_name) {
            // $tag = Tag::where("tag_name", $tag_name)->first();
            // if ($tag) {
            //     $tag_ids[] = $tag->id;
            // } else {
            //     $new_tag = Tag::create(["tag_name" => $tag_name]);
            //     $tag_ids[] = $new_tag->id;
            // }
            $tag = Tag::firstOrCreate(['tag_name' => $tag_name]);
            $tag_ids[] = $tag->id;
        }
        //cara mass assignment
        $pertanyaan = Pertanyaan::create([
            'judul' => $request['judul'],
            'isi' => $request['isi']
        ]);

        $pertanyaan->tags()->sync($tag_ids);
        // $profil = Auth::profil();
        // $profil->pertanyaan()->save($pertanyaan);

        //explode dulu untuk ngubah request tag jadi array
        //looping ke array tags tadi bikin arr penampung
        //tiap looping lakukan pengecekan apa sdh ada tagnya
        //kl sdh ada ambil idnya
        //kl belum ada simpan dulu di tagnya lalu ambil idnya
        //tampung di array penampung
        Alert::success('Berhasil', 'Berhasil Menambah Pertanyaan Baru');
        return redirect('/pertanyaan');
    }

    public function readData()
    {
        // $pertanyaan = DB::table('pertanyaan')->get();

        $pertanyaan = Pertanyaan::all();

        return view('pertanyaan.listPertanyaan', compact('pertanyaan'));
    }

    public function readDataById($id)
    {
        // $pertanyaan = DB::table('pertanyaan')->where('pertanyaan_id', $id)->first(); //bisa first() ato get()

        $pertanyaan = Pertanyaan::find($id);

        return view('pertanyaan.detailPertanyaan', compact('pertanyaan'));
    }

    public function editForm($id)
    {
        // $pertanyaan = DB::table('pertanyaan')->where('pertanyaan_id', $id)->first();

        $pertanyaan = Pertanyaan::where('pertanyaan_id', $id)->first();
        return view('pertanyaan.editPertanyaan', compact('pertanyaan'));
    }

    public function updateData($id, Request $request)
    {

        // $query = DB::table('pertanyaan')
        //     ->where('pertanyaan_id', $id)
        //     ->update([
        //         'judul' => $request['judul'],
        //         'isi' => $request['isi']
        //     ]);

        $update = Pertanyaan::where('pertanyaan_id', $id)->update([
            'judul' => $request['judul'],
            'isi' => $request['isi']
        ]);

        return redirect('/pertanyaan')->with('success', 'Berhasil update Pertanyaan');
    }

    public function deleteData($id)
    {
        // $query = DB::table('pertanyaan')->where('pertanyaan_id', $id)->delete();

        Pertanyaan::destroy($id);

        return redirect('/pertanyaan')->with('success', 'Berhasil hapus Pertanyaan');
    }
}
