<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jawaban extends Model
{
    protected $table = "jawaban";//kalo mau override nama table
    protected $primaryKey = "jawaban_id"; //kalo mau override nama PK

    public function pertanyaan()
    {
        return $this->hasOne('App\Pertanyaan','jawaban_tepat_id','jawaban_id');
    }
    public function pertanyaans()
    {
        return $this->belongsTo('App\Pertanyaan');
    }
}
