<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('wwelcome');
});

// Route::get('/register', function () {
//     return view('register');
// });

// Route::get('/welcome/{namaDepan}/{namaBelakang}', function ($namaDepan,$namaBelakang) {
//     return view("welcome", ["namaDepan" => $namaDepan, "namaBelakang" => $namaBelakang]);
// });

// Route::get('/', 'HomeController@home');
// Route::get('/register', 'AuthController@register');
// Route::get('/welcome', 'AuthController@welcome');
// Route::post('/welcome', 'AuthController@welcome_post');

// Route::get('/', function () {
//     return view('data_chart');
// });

// Route::get('/data-tables', function () {
//     return view('data_table');
// });

Route::get('/pertanyaan', 'PertanyaanController@readData'); //->name('pertanyaan.index');untuk ngasih nama di route:list
Route::get('/pertanyaan/create', 'PertanyaanController@createForm'); //->middleware('auth');untuk ngasih auth di sini
Route::post('/pertanyaan', 'PertanyaanController@createData');
Route::get('/pertanyaan/{id}', 'PertanyaanController@readDataById');
Route::get('/pertanyaan/{id}/edit', 'PertanyaanController@editForm');
Route::put('/pertanyaan/{id}', 'PertanyaanController@updateData');
Route::delete('/pertanyaan/{id}', 'PertanyaanController@deleteData');

Route::resource('pertanyaanku', 'PertanyaankuController');
// kl mau panggil pake namenya waktu di blade pakai href="{{route('pertanyaanku.create')}}" 
// atau href ="{{route('pertanyaanku.show',['pertanyaanku' => $xxx->id])}}"

Route::get('/test-dompdf', function () {
    $pdf = App::make('dompdf.wrapper');
    $pdf->loadHTML('<h1>Test</h1>');
    return $pdf->stream();
});

Route::get('/test-dompdf-2', 'PdfController@test');

Route::get('/test-excel', 'PostController@export');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});
