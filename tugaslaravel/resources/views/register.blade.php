<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Form Page</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label for="firstName">First name:</label><br /><br />
        <input type="text" id="firstName" name="firstName" /><br /><br />
        <label for="lastName">Last name:</label><br /><br />
        <input type="text" id="lastName" name="lastName" /><br /><br />

        <label>Gender:</label><br /><br />
        <input type="radio" name="gender" value="Male" /> Male<br />
        <input type="radio" name="gender" value="Female" /> Female<br />
        <input type="radio" name="gender" value="Other" /> Other<br /><br />

        <label for="nationality">Nationality:</label><br /><br />
        <select name="nationality" id="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Korean">Korean</option>
            <option value="Japanese">Japanese</option>
            <option value="American">American</option>
        </select><br /><br />

        <label>Language Spoken:</label><br /><br />
        <input type="checkbox" name="language" value="BahasaIndonesia" /> Bahasa
        Indonesia<br />
        <input type="checkbox" name="language" value="English" /> English<br />
        <input type="checkbox" name="language" value="Other" /> Other<br /><br />

        <label for="bio">Bio:</label><br /><br />
        <textarea name="bio" id="bio" rows="10" cols="30"></textarea><br />

        <input type="submit" value="Sign Up" />
    </form>
</body>

</html>