@extends('master2')

@section('content')
    <div class="mt-3 mx-3">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Pertanyaan Table</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                <a href="/pertanyaan/create" class="btn btn-primary mb-3">Create New Question</a>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Judul</th>
                            <th>Isi</th>
                            <th style="width: 40px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{-- @foreach ($pertanyaan as $key => $item)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $item->judul }}</td>
                                <td>{{ $item->isi }}</td>
                                <td> </td>
                            </tr>
                        @endforeach --}}
                        @forelse ($pertanyaan as $key => $item)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $item->judul }}</td>
                                <td>{!! $item->isi !!}</td>
                                <td style="display: flex"> 
                                    <a href="/pertanyaan/{{ $item->pertanyaan_id }}" class="btn btn-info btn-sm">Detail</a> 
                                    <a href="/pertanyaan/{{ $item->pertanyaan_id }}/edit" class="btn btn-default btn-sm">Edit</a> 
                                    <form action="/pertanyaan/{{ $item->pertanyaan_id }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                    <input type="submit" value="delete" class="btn btn-danger btn-sm">
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4" align="center">No Question</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>

    </div>
@endsection
