@extends('master2')

@section('content')
    <div class="mt-3 mx-3">
        <h4>{{ $pertanyaan->judul }}</h4>
        <p>{{ $pertanyaan->isi }}</p>
        <div>
            Tags :
            @forelse($pertanyaan->tags as $tag)
                <button class="btn btn-primary">{{ $tag->tag_name }}</button>
                @empty
                No Tags    
            @endforelse
        </div>
    </div>
@endsection
