@extends('master2')

@section('content')
    <div class="mx-2 mt-2">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Edit Question {{ $pertanyaan->pertanyaan_id }}</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="/pertanyaan/{{ $pertanyaan->pertanyaan_id }}" method="POST">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <input type="text" class="form-control" id="judul" value="{{ old('judul', $pertanyaan->judul) }}"
                            name="judul" placeholder="Masukkan Judul" required>
                        @error('judul')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="isi">Isi</label>
                        <input type="text" class="form-control" id="isi" value="{{ old('isi', $pertanyaan->isi) }}"
                            name="isi" placeholder="Masukkan Isi" required>
                        @error('isi')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>

    </div>
@endsection
